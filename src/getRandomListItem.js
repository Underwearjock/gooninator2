const getRandomIndex = (list) => {
  return Math.floor(Math.random()*list.length)
};

export default (list) => {
  return list[getRandomIndex(list)];
};
